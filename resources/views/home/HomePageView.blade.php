<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body class="font-dm">

{{-- Navbar --}}
<section>
        <nav class="bg-pinkPrime backdrop-blur-lg">
            <div class="flex flex-wrap items-center justify-between max-w-screen-xl p-4 mx-auto">
                <a href="https://flowbite.com/" class="flex items-center">
                    <img src="https://flowbite.com/docs/images/logo.svg" class="h-8 mr-3" alt="Flowbite Logo" />
                    <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">mailove</span>
                </a>
                <div class="flex md:order-2">
                    <button type="button"
                        class="hidden md:inline-flex button-nav text-white hover:bg-darkPrime hover:text-white mr-3">Login
                    </button>
                    <button type="button"
                        class="hidden mr-3 text-white md:inline-flex button-nav bg-pinkPrime border-2 border-white hover:bg-darkPrime hover:border-darkPrime">Register
                    </button>

                    <button data-collapse-toggle="navbar-cta" type="button"
                        class="inline-flex items-center p-2 text-sm text-white rounded-lg md:hidden hover:bg-darkPrime focus:outline-none focus:ring-2 focus:ring-gray-200 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                        aria-controls="navbar-cta" aria-expanded="false">
                        <span class="sr-only">Open main menu</span>
                        <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                                clip-rule="evenodd"></path>
                        </svg>
                    </button>
                </div>
                <div class="items-center bg-pinkPrime border border-white justify-between hidden w-full text-lg rounded-lg mt-4 md:mt-0 md:bg-pinkPrime md:border-none md:flex md:w-auto md:order-1" id="navbar-cta">
                    <ul
                        class="flex flex-col p-4 font-medium rounded-lg md:p-0 md:flex-row md:space-x-8 md:mt-0 md:border-0">
                        <li>
                            <a href="#"
                                class="menuNav"
                                aria-current="page">Home</a>
                        </li>
                        <li>
                            <a href="#"
                                class="menuNav">Tamplate</a>
                        </li>
                        <li>
                            <a href="#"
                                class="menuNav">Price</a>
                        </li>
                        <li>
                            <a href="#"
                                class="menuNav">Testimoni</a>
                        </li>
                        <li>
                            <a href="#"
                                class="menuNav">Affiliate</a>
                        </li>
                        <div class="flex gap-4 font-medium">
                            <div class="w-full">
                                <button type="button"
                                class="md:hidden inline-flex button-nav2 text-white hover:bg-darkPrime hover:text-white mr-3">Login
                                </button>
                            </div>
                            <div class="w-full">
                                <button type="button"
                                class="inline-flex mr-3 text-white md:hidden button-nav2 bg-pinkPrime border-2 border-white hover:bg-darkPrime hover:border-darkPrime">Register
                                </button>
                            </div>
                        </div>

                    </ul>
                </div>
            </div>
        </nav>
</section>
{{-- Navbar End --}}

{{-- Hero --}}
<section class="bg-gradient-to-b from-pinkPrime to-pink-700 pb-10">
<div class="flex flex-col-reverse items-center mx-auto space-y-0 md:flex-row md:justify-evenly md:space-y-0 px-4 md:py-8 lg:py-20">
    <div class="justify-center md:justify-start px-4 md:px-0 py-8 text-center md:text-left lg:py-0 md:w-1/2">
        <h1 class="mb-4 text-4xl font-extrabold leading-none tracking-wide text-gray-950 md:text-4xl lg:text-6xl">Create an unforgettable impression with advanced digital invitations</h1>
        <p class="font-plus mb-8 text-lg font-normal tracking-wide text-white lg:text-2xl">Present a special moment with an unforgettable impression for your guests!</p>
        <div class="justify-center md:justify-start">
            <a href="#" class="act-btn bg-white hover:text-darkPrime hover:bg-secondary">
                Get Started
            </a>
        </div>
    </div>
    <div>
        <img src="{{asset('img/hero-mailove.png')}}" alt="image" class="w-full " class="md:w-1/2">
    </div>
</div>
</section>
{{-- Hero Ends --}}



{{-- Features --}}
<section>
    <div class="bg-gradient-to-br from-white from-40% via-pinkPrime to-darkPrime justify-center items-center py-10 px-4 md:p-10 lg:p-20">
        <div class="text-center items-center justify-center px-4">
            <h1 class="text-gray-950 font-semibold text-2xl font-dm md:text-3xl lg:text-4xl">
                Features
            </h1>
            <p class="mt-2 md:m-4 lg:mt-5 lg:mx-[160px] text-center items-center font-plus text-gray-700 text-base md:text-lg lg:text-xl">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. In dicta repellendus, est dolor corporis mollitia iste ratione quo suscipit quaerat ab nostrum optio dolore sunt quos! Quibusdam quaerat fugit nam!
            </p>
        </div>
        <div class="flex-col md:flex-row justify-content items-center my-4 md:my-6 lg:my-8 gap-4 lg:gap-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
            <div class="bg-white border border-gray-200 shadow-md rounded-lg p-5 md:p-6">
                <div class="flex align-middle mb-2 gap-2 items-center">
                    <div class="p-2 bg-pinkPrime rounded-[4px]">
                        <img src="{{asset('icon/smartphone-line.svg')}}" alt="hp" width="28px" class="">
                    </div>
                    <p class="text-gray-950 text-lg lg:text-xl  tracking-wide">Barcode attendence</p>
                </div>
                <p class="font-plus text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div class="bg-white border border-gray-200 shadow-md rounded-lg p-5 md:p-6">
                <div class="flex align-middle mb-2 gap-2 items-center">
                    <div class="p-2 bg-pinkPrime rounded-[4px]">
                        <img src="{{asset('icon/smartphone-line.svg')}}" alt="hp" width="28px" class="">
                    </div>
                    <p class="text-gray-950 text-lg lg:text-xl  tracking-wide">Barcode attendence</p>
                </div>
                <p class="font-plus text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div class="bg-white border border-gray-200 shadow-md rounded-lg p-5 md:p-6">
                <div class="flex align-middle mb-2 gap-2 items-center">
                    <div class="p-2 bg-pinkPrime rounded-[4px]">
                        <img src="{{asset('icon/smartphone-line.svg')}}" alt="hp" width="28px" class="">
                    </div>
                    <p class="text-gray-950 text-lg lg:text-xl  tracking-wide">Barcode attendence</p>
                </div>
                <p class="font-plus text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div class="bg-white border border-gray-200 shadow-md rounded-lg p-5 md:p-6">
                <div class="flex align-middle mb-2 gap-2 items-center">
                    <div class="p-2 bg-pinkPrime rounded-[4px]">
                        <img src="{{asset('icon/smartphone-line.svg')}}" alt="hp" width="28px" class="">
                    </div>
                    <p class="text-gray-950 text-lg lg:text-xl  tracking-wide">Barcode attendence</p>
                </div>
                <p class="font-plus text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div class="bg-white border border-gray-200 shadow-md rounded-lg p-5 md:p-6">
                <div class="flex align-middle mb-2 gap-2 items-center">
                    <div class="p-2 bg-pinkPrime rounded-[4px]">
                        <img src="{{asset('icon/smartphone-line.svg')}}" alt="hp" width="28px" class="">
                    </div>
                    <p class="text-gray-950 text-lg lg:text-xl  tracking-wide">Barcode attendence</p>
                </div>
                <p class="font-plus text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div class="bg-white border border-gray-200 shadow-md rounded-lg p-5 md:p-6">
                <div class="flex align-middle mb-2 gap-2 items-center">
                    <div class="p-2 bg-pinkPrime rounded-[4px]">
                        <img src="{{asset('icon/smartphone-line.svg')}}" alt="hp" width="28px" class="">
                    </div>
                    <p class="text-gray-950 text-lg lg:text-xl  tracking-wide">Barcode attendence</p>
                </div>
                <p class="font-plus text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
        </div>
    </div>
</section>
{{-- Features Ends --}}
<section>
    <div class="justify-center items-center py-10 px-4 md:p-10 lg:p-20">
        <div class="text-center items-center justify-center px-4 mb-4 md:mb-6 lg:mb-8">
            <h1 class="text-gray-950 font-semibold text-2xl font-dm md:text-3xl lg:text-4xl">
                Tamplate
            </h1>
        </div>
        <div class="my-4 md:my-6 lg:my-8 md:flex-row gap-4 md:gap-6 lg:gap-8 grid grid-cols-1 md:grid-cols-2">
            <div class="flex flex-col gap-4 md:gap-6 lg:gap-8 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 pb-4 md:pb-6 lg:pb-8 ">
                <img src="{{asset('img/product.jpg')}}" alt="image" class="w-full rounded-t-lg rounde" class="md:w-1/2">
                <h1 class="mx-4 md:mx-6 lg:mx-8 text-gray-950 font-semibold text-xl md:text-2xl">Desain 1</h1>
                <div class="flex gap-2 lg:gap-4 mx-4 md:mx-6 lg:mx-8">
                    <button type="button" class="text-pinkPrime border border-pinkPrime px-auto hover:text-white button-prod">See Detail</button>
                    <button type="button" class="text-white bg-pinkPrime px-auto button-prod">Create Undangan</button>
                </div>
            </div>
            <div class="flex flex-col gap-4 md:gap-6 lg:gap-8 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 pb-4 md:pb-6 lg:pb-8 ">
                <img src="{{asset('img/product.jpg')}}" alt="image" class="w-full rounded-t-lg rounde" class="md:w-1/2">
                <h1 class="mx-4 md:mx-6 lg:mx-8 text-gray-950 font-semibold text-xl md:text-2xl">Desain 1</h1>
                <div class="flex gap-2 lg:gap-4 mx-4 md:mx-6 lg:mx-8">
                    <button type="button" class="text-pinkPrime border border-pinkPrime px-auto hover:text-white button-prod">See Detail</button>
                    <button type="button" class="text-white bg-pinkPrime px-auto button-prod">Create Undangan</button>
                </div>
            </div>
            <div class="flex flex-col gap-4 md:gap-6 lg:gap-8 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 pb-4 md:pb-6 lg:pb-8 ">
                <img src="{{asset('img/product.jpg')}}" alt="image" class="w-full rounded-t-lg rounde" class="md:w-1/2">
                <h1 class="mx-4 md:mx-6 lg:mx-8 text-gray-950 font-semibold text-xl md:text-2xl">Desain 1</h1>
                <div class="flex gap-2 lg:gap-4 mx-4 md:mx-6 lg:mx-8">
                    <button type="button" class="text-pinkPrime border border-pinkPrime px-auto hover:text-white button-prod">See Detail</button>
                    <button type="button" class="text-white bg-pinkPrime px-auto button-prod">Create Undangan</button>
                </div>
            </div>
            <div class="flex flex-col gap-4 md:gap-6 lg:gap-8 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 pb-4 md:pb-6 lg:pb-8 ">
                <img src="{{asset('img/product.jpg')}}" alt="image" class="w-full rounded-t-lg rounde" class="md:w-1/2">
                <h1 class="mx-4 md:mx-6 lg:mx-8 text-gray-950 font-semibold text-xl md:text-2xl">Desain 1</h1>
                <div class="flex gap-2 lg:gap-4 mx-4 md:mx-6 lg:mx-8">
                    <button type="button" class="text-pinkPrime border border-pinkPrime px-auto hover:text-white button-prod">See Detail</button>
                    <button type="button" class="text-white bg-pinkPrime px-auto button-prod">Create Undangan</button>
                </div>
            </div>



        </div>
    </div>
</section>
{{-- Products --}}

{{-- Product Ends --}}

{{-- Pricing --}}
<section>
    <div class="bg-gradient-to-b from-white to bg-secondary mx-auto justify-center items-center px-10 md:px-16 lg:px-24 py-8 md:py-10 lg:py-20 gap-4">
        <div class="text-center items-center justify-center">
            <h1 class="text-gray-950 font-semibold text-2xl font-dm md:text-3xl lg:text-4xl">
                Our Package
            </h1>
            <p class="mt-2 md:m-4 lg:mt-5 lg:mx-[160px] text-center items-center font-plus text-gray-700 text-base md:text-lg lg:text-xl">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus nesciunt consequatur, ab perspiciatis tenetur voluptas, dolor quos corporis vel
            </p>
        </div>
        <div class="flex flex-col my-4 md:my-6 lg:my-8 md:flex-row gap-4 md:gap-6 lg:gap-8 font-plus">
            <div class="w-full p-4 bg-white border border-gray-200 rounded-lg shadow-lg sm:p-8 hover:scale-105 transition duration-300 ease-in ">
            <h5 class="mb-4 text-xl lg:text-2xl font-dm font-semibold text-grey-600">Standard plan</h5>
            <div class="flex items-baseline text-darkPrime dark:text-white">
                <span class="text-3xl font-semibold mr-2">Rp</span>
                <span class="text-5xl font-extrabold tracking-tight">250.000</span>
            </div>
            <!-- List -->
            <ul role="list" class="space-y-5 my-7">
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">2 team members</span>
            </li>
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">20GB Cloud storage</span>
            </li>
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Integration help</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Sketch Files</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">API Access</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Complete documentation</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">24×7 phone & email support</span>
            </li>
            </ul>
            <button type="button" class="text-white bg-pinkPrime hover:bg-darkPrime focus:ring-4 focus:outline-none focus:ring-secondary dark:focus:ring-secondary font-medium rounded-lg text-[14px] md:text-[16px] lg:text-[20px] px-auto py-3 inline-flex justify-center w-full text-center">Choose plan</button>
            </div>
            <div class="w-full p-4 bg-white border border-gray-200 rounded-lg shadow-lg sm:p-8 hover:scale-105 transition duration-300 ease-in ">
            <h5 class="mb-4 text-xl lg:text-2xl font-dm font-semibold text-grey-600 ">Standard plan</h5>
            <div class="flex items-baseline text-darkPrime dark:text-white">
                <span class="text-3xl font-semibold mr-2">Rp</span>
                <span class="text-5xl font-extrabold tracking-tight">250.000</span>
            </div>
            <!-- List -->
            <ul role="list" class="space-y-5 my-7">
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">2 team members</span>
            </li>
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">20GB Cloud storage</span>
            </li>
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Integration help</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Sketch Files</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">API Access</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Complete documentation</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">24×7 phone & email support</span>
            </li>
            </ul>
            <button type="button" class="text-white bg-pinkPrime hover:bg-darkPrime focus:ring-4 focus:outline-none focus:ring-secondary dark:focus:ring-secondary font-medium rounded-lg text-[14px] md:text-[16px] lg:text-[20px] px-auto py-3 inline-flex justify-center w-full text-center">Choose plan</button>
            </div>
            <div class="w-full p-4 bg-white border border-gray-200 rounded-lg shadow-lg sm:p-8 hover:scale-105 transition duration-300 ease-in ">
            <h5 class="mb-4 text-xl lg:text-2xl font-dm font-semibold text-grey-600 ">Standard plan</h5>
            <div class="flex items-baseline text-darkPrime dark:text-white">
                <span class="text-3xl font-semibold mr-2">Rp</span>
                <span class="text-5xl font-extrabold tracking-tight">250.000</span>
            </div>
            <!-- List -->
            <ul role="list" class="space-y-5 my-7">
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">2 team members</span>
            </li>
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">20GB Cloud storage</span>
            </li>
            <li class="flex space-x-3">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-pinkPrime dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Integration help</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Sketch Files</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">API Access</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">Complete documentation</span>
            </li>
            <li class="flex space-x-3 line-through decoration-gray-500">
                <!-- Icon -->
                <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-gray-400 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                <span class="text-base font-normal leading-tight text-gray-500">24×7 phone & email support</span>
            </li>
            </ul>
            <button type="button" class="text-white bg-pinkPrime hover:bg-darkPrime focus:ring-4 focus:outline-none focus:ring-secondary dark:focus:ring-secondary font-medium rounded-lg text-[14px] md:text-[16px] lg:text-[20px] px-auto py-3 inline-flex justify-center w-full text-center">Choose plan</button>
            </div>
        </div>
    </div>
</section>
{{-- Pricing Ends --}}

{{-- Testimonial --}}
<section>
    <div class="mx-auto justify-center items-center px-10 md:px-16 lg:px-24 py-8 md:py-10 lg:py-20 gap-4">
        <div class="text-center items-center justify-center">
            <h1 class="text-gray-950 font-semibold text-2xl font-dm md:text-3xl lg:text-4xl">
                Testimonial
            </h1>
            <p class="mt-2 md:m-4 lg:mt-5 lg:mx-[160px] text-center items-center font-plus text-gray-700 text-base md:text-lg lg:text-xl">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus nesciunt consequatur, ab perspiciatis tenetur voluptas, dolor quos corporis vel
            </p>
        </div>
        <div class="flex flex-col my-4 md:my-6 lg:my-8 md:flex-row gap-4 md:gap-6 lg:gap-8 font-plus items-center">
            <div class="hidden md:flex order-2 md:order-none rounded-full p-2 shadow-lg items-center border border-gray-200 hover:shadow-xl hover:scale-110 transition duration-300 ease-in cursor-pointer">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-950" viewBox="0 0 24 24"><path d="M10.8284 12.0007L15.7782 16.9504L14.364 18.3646L8 12.0007L14.364 5.63672L15.7782 7.05093L10.8284 12.0007Z" fill="currentColor" ></path></svg>
            </div>
            <div id="card-testi" class="order-1 md:order-none bg-white flex flex-col border border-gray-200 shadow-md rounded-lg p-5 md:p-6 gap-4">
                <div class="flex items-center gap-4">
                    <img src="{{asset('img/pp.png')}}" alt="pp" width="70px" class="rounded-full">
                    <div>
                        <h1 class="font-dm font-semibold text-gray-950 tracking-wide text-xl">Bisma Mahendra</h1>
                        <h2 class="text-gray-700">Enterpreneur</h2>
                    </div>
                </div>
                <div class="flex gap-2">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                </div>
                <p class="text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div id="card-testi" class="hidden bg-white md:flex md:flex-col border border-gray-200 shadow-md rounded-lg p-5 md:p-6 gap-4">
                <div class="flex items-center gap-4">
                    <img src="{{asset('img/pp.png')}}" alt="pp" width="70px" class="rounded-full">
                    <div>
                        <h1 class="font-dm font-semibold text-gray-950 tracking-wide text-xl">Bisma Mahendra</h1>
                        <h2 class="text-gray-700">Enterpreneur</h2>
                    </div>
                </div>
                <div class="flex gap-2">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                </div>
                <p class="text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div id="card-testi" class="hidden bg-white md:flex md:flex-col border border-gray-200 shadow-md rounded-lg p-5 md:p-6 gap-4">
                <div class="flex items-center gap-4">
                    <img src="{{asset('img/pp.png')}}" alt="pp" width="70px" class="rounded-full">
                    <div>
                        <h1 class="font-dm font-semibold text-gray-950 tracking-wide text-xl">Bisma Mahendra</h1>
                        <h2 class="text-gray-700">Enterpreneur</h2>
                    </div>
                </div>
                <div class="flex gap-2">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-pinkPrime" viewBox="0 0 24 24"><path d="M12.0006 18.26L4.94715 22.2082L6.52248 14.2799L0.587891 8.7918L8.61493 7.84006L12.0006 0.5L15.3862 7.84006L23.4132 8.7918L17.4787 14.2799L19.054 22.2082L12.0006 18.26Z" fill="currentColor"></path></svg>
                </div>
                <p class="text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore nobis corrupti consequatur, voluptatibus quos enim nesciunt porro a distinctio praesentium.</p>
            </div>
            <div class="flex order-2 md:order-none gap-3">
                <div class="md:hidden order-2 md:order-none rounded-full p-2 shadow-lg items-center border border-gray-200 hover:shadow-xl hover:scale-110 transition duration-300 ease-in cursor-pointer">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-950" viewBox="0 0 24 24"><path d="M10.8284 12.0007L15.7782 16.9504L14.364 18.3646L8 12.0007L14.364 5.63672L15.7782 7.05093L10.8284 12.0007Z" fill="currentColor" ></path></svg>
                </div>
                <div class="order-3 md:order-none rounded-full p-2 shadow-lg items-center border border-gray-200 hover:shadow-xl hover:scale-110 transition duration-300 ease-in cursor-pointer">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-950" viewBox="0 0 24 24"><path d="M13.1714 12.0007L8.22168 7.05093L9.63589 5.63672L15.9999 12.0007L9.63589 18.3646L8.22168 16.9504L13.1714 12.0007Z" fill="currentColor"></path></svg>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- Testimonial Ends --}}

    <!-- Footer -->
    <section id="footter" class="bg-darkPink py-4 lg:py-16 font-dm">
        <div class="flex flex-col px-6 mx-auto my-10 space-y-8 justify-between md:space-y-0 text-white md:px-20 lg:px-32 md:flex-row md:space-x-16">
            <div class=" flex flex-col md:flex-row space-y-2 md:space-y-0 lg:space-x-2 lg:w-1/2">
                <div class="-ml-4 md:ml-0 lg:-mt-2">
                    <img src="src/logo/logo-white.svg" alt="mailove" width="100px" class="">
                </div>
                <div class="space-y-2">
                    <h3 class="text-lg lg:text-xl tracking-wide">PT. Mailove</h3>
                    <p class=" max-w-xs font-light text-sm font-plus">Make Your Wedding Memorable</p>
                    <div class="mt-2">
                        <div class="flex items-center">
                              <!-- Facebook -->
                            <a class="p-3 mr-2 rounded-lg flex justify-center items-center bg-white text-greenPrime hover:bg-greenSecond hover:text-white transition duration-700" href="https://instagram.com/frimantara.id?igshid=YmMyMTA2M2Y=" target="_blank" >
                                <svg role="img" class="w-8 h-8 text-pinkPrime" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Facebook</title><path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z" fill="currentColor" /></svg>
                            </a>
                              <!-- Insta -->
                            <a class="p-3 mr-2 rounded-lg flex justify-center items-center bg-white text-greenPrime hover:bg-greenSecond hover:text-white transition duration-700" href="https://instagram.com/frimantara.id?igshid=YmMyMTA2M2Y=" target="_blank" >
                                <svg role="img" width="28" class="w-8 h-8 text-pinkPrime" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Instagram</title><path d="M12 0C8.74 0 8.333.015 7.053.072 5.775.132 4.905.333 4.14.63c-.789.306-1.459.717-2.126 1.384S.935 3.35.63 4.14C.333 4.905.131 5.775.072 7.053.012 8.333 0 8.74 0 12s.015 3.667.072 4.947c.06 1.277.261 2.148.558 2.913.306.788.717 1.459 1.384 2.126.667.666 1.336 1.079 2.126 1.384.766.296 1.636.499 2.913.558C8.333 23.988 8.74 24 12 24s3.667-.015 4.947-.072c1.277-.06 2.148-.262 2.913-.558.788-.306 1.459-.718 2.126-1.384.666-.667 1.079-1.335 1.384-2.126.296-.765.499-1.636.558-2.913.06-1.28.072-1.687.072-4.947s-.015-3.667-.072-4.947c-.06-1.277-.262-2.149-.558-2.913-.306-.789-.718-1.459-1.384-2.126C21.319 1.347 20.651.935 19.86.63c-.765-.297-1.636-.499-2.913-.558C15.667.012 15.26 0 12 0zm0 2.16c3.203 0 3.585.016 4.85.071 1.17.055 1.805.249 2.227.415.562.217.96.477 1.382.896.419.42.679.819.896 1.381.164.422.36 1.057.413 2.227.057 1.266.07 1.646.07 4.85s-.015 3.585-.074 4.85c-.061 1.17-.256 1.805-.421 2.227-.224.562-.479.96-.899 1.382-.419.419-.824.679-1.38.896-.42.164-1.065.36-2.235.413-1.274.057-1.649.07-4.859.07-3.211 0-3.586-.015-4.859-.074-1.171-.061-1.816-.256-2.236-.421-.569-.224-.96-.479-1.379-.899-.421-.419-.69-.824-.9-1.38-.165-.42-.359-1.065-.42-2.235-.045-1.26-.061-1.649-.061-4.844 0-3.196.016-3.586.061-4.861.061-1.17.255-1.814.42-2.234.21-.57.479-.96.9-1.381.419-.419.81-.689 1.379-.898.42-.166 1.051-.361 2.221-.421 1.275-.045 1.65-.06 4.859-.06l.045.03zm0 3.678c-3.405 0-6.162 2.76-6.162 6.162 0 3.405 2.76 6.162 6.162 6.162 3.405 0 6.162-2.76 6.162-6.162 0-3.405-2.76-6.162-6.162-6.162zM12 16c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm7.846-10.405c0 .795-.646 1.44-1.44 1.44-.795 0-1.44-.646-1.44-1.44 0-.794.646-1.439 1.44-1.439.793-.001 1.44.645 1.44 1.439z" fill="currentColor" /></svg>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="space-y-2 lg:w-1/3 ">
                <h3 class="text-lg tracking-wide">Contact</h3>
                <p class=" max-w-sm font-light text-sm font-plus">+62 821 1213 1830</p>
                <p class=" max-w-sm font-light text-sm font-plus">mailove@gmail.com</p>
            </div>
            <div class="space-y-2 lg:w-1/3 ">
                <h3 class="text-lg tracking-wide">Location</h3>
                <p class=" max-w-sm font-light text-sm font-plus">Jakarta, Indonesia</p>
            </div>
        </div>
    </section>
    <!-- Footer Ends -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
</body>

</html>
