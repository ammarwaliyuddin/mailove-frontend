/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
      ],
  theme: {
    extend: {

        fontFamily: {
            'dm': ['DM Serif Display', 'serif'],
            'plus': ['Plus Jakarta Sans', 'sans-serif'],
        },
        colors: {
            pinkPrime: '#f64a8a',
            darkPrime: '#da135c',
            secondary: '#fca3b7',
            tertiary: '#a2eae0',
            darkPink: '#990038',
        }
    },
  },
  plugins: [],
}

